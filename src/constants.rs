pub const FALSE: isize = 0;
pub const TRUE: isize = -1;

pub const TERM_WIDTH: u16 = 80;
