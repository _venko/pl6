#![allow(dead_code, unused)]

use std::fmt;
use std::fmt::Display;
use std::ops::{Index, IndexMut};

use std::io::{stdout, Write};
use termion::cursor::{DetectCursorPos, Goto, Restore, Right, Save, Up};
use termion::raw::IntoRawMode;
use termion::style::{Bold, Invert, Italic, NoBold, NoInvert, NoItalic};

// TODO: Can rustyline share the same stdout as the Vm to make text management easier?
use rustyline::config::{Builder, Config, EditMode, HistoryDuplicates};
use rustyline::error::ReadlineError;
use rustyline::history::DefaultHistory;
use rustyline::{DefaultEditor, Editor};

mod constants;
mod op;
mod stack;
mod types;

use constants::*;
use op::*;
use stack::*;
use types::*;

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
enum Instruction {
    Native(Op),
    Forth(WordAddr),
}

#[derive(Debug, Clone)]
struct Word {
    name: String,
    code: Vec<Instruction>,
    data: Vec<VmWord>,
    immediate: bool,
    hidden: bool,
}

/// A dictionary defining a set of "words", or functions>.
///
/// A dictionary stores all or some of the state of an executing
/// PL6 program.
#[derive(Debug, Default)]
struct Dictionary {
    /// An optional specifier to qualify a dictionary as a separate namespace.
    ///
    /// The default specifier is "" and represents the global namespace that
    /// programs launch into. Other dictionaries can be imported (TODO) into the
    /// global namespace as code modules. Code modules should use specifiers to
    /// isolate their words behind an import statement.
    specifier: String,

    /// A region of memory allocated for storing strings in the PL6 virtual
    /// machine.
    ///
    /// All strings are, in conventional terms, "heap allocated", and their
    /// representation on the stack is the pair ( addr len ). The addr in a
    /// string pair is an index into this memory space.
    strings: Vec<String>,

    /// The actual contents of the dictionary, organized as a set of words
    /// queryable by a string name.
    words: Vec<Word>,
}

impl Index<usize> for Dictionary {
    type Output = Word;

    fn index(&self, idx: usize) -> &Self::Output {
        &self.words[idx]
    }
}

impl IndexMut<usize> for Dictionary {
    fn index_mut(&mut self, idx: usize) -> &mut Self::Output {
        &mut self.words[idx]
    }
}

impl Index<CodePtr> for Dictionary {
    type Output = Instruction;

    fn index(&self, ptr: CodePtr) -> &Self::Output {
        &self.words[ptr.index].code[ptr.offset]
    }
}

impl Dictionary {
    pub fn lookup_word(&self, name: &String) -> Option<WordAddr> {
        self.words
            .iter()
            .enumerate()
            .rev()
            .find(|(_, word)| word.name.to_uppercase() == *name.to_uppercase())
            .map(|(idx, _)| idx)
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
enum VmMode {
    Interpreting,
    Compiling,
}

impl Default for VmMode {
    fn default() -> VmMode {
        VmMode::Interpreting
    }
}

enum VmError {
    /// Indicates that there are too few items on the stack to complete the
    /// current operation (code_ptr).
    StackUnderflow,

    /// Indicates that a string lookup was performed using an invalid index
    /// into the dictionary's string region.
    InvalidStringIndex,

    /// Indicates that a word lookup was performed on a string with no
    /// corresponding word in the dictionary.
    UnknownWord,

    /// Indicates that a parsing word expected to find a word in user input but
    /// instead encountered an empty string.
    ZeroLengthWord,

    /// Indicates that a compile-time only word was encountered while the VM
    /// was in interpret mode.
    CompileOnlyWord,

    /// Indicates a word that should not be reachable through program execution.
    UnreachableWord,
}

/// Status codes for
enum VmErrorHandleResult {
    /// Crash the program.
    Panic,
    /// Return to the repl, discard any stack elements and hide any
    /// currently-compiling words.
    Reset,
    /// Resume execution at the error-site, assuming that the error conditions
    /// have been resolved through user input.
    Continue,
}

// Alias to make terminal Debug-able
struct VmTerminal(termion::raw::RawTerminal<std::io::Stdout>);
impl fmt::Debug for VmTerminal {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
        write!(f, "termion::raw::RawTerminal<std::io::Stdout>")
    }
}

#[derive(Debug)]
struct Vm {
    dict: Dictionary,
    stack: Stack<VmWord>,
    return_stack: Stack<CodePtr>,
    input: String,

    code_ptr: CodePtr,
    input_ptr: usize,

    mode: VmMode,
    latest_word: Option<usize>,

    rl_editor: Editor<(), DefaultHistory>,
    stdout: VmTerminal,
    show_status: bool,
    prev_line_offset: u16,

    debug: bool,
}

impl Default for Vm {
    fn default() -> Vm {
        Vm {
            dict: Dictionary::default(),
            stack: Stack::default(),
            return_stack: Stack::default(),
            input: String::default(),
            code_ptr: CodePtr::default(),
            input_ptr: 0,
            mode: VmMode::default(),
            latest_word: Option::default(),
            rl_editor: Editor::<(), DefaultHistory>::with_config(
                Builder::new()
                    .edit_mode(EditMode::Vi)
                    .auto_add_history(true)
                    .tab_stop(2)
                    .indent_size(2)
                    .history_ignore_dups(true)
                    .ok()
                    .unwrap()
                    .build(),
            )
            .ok()
            .unwrap(),
            stdout: VmTerminal(stdout().into_raw_mode().unwrap()),
            show_status: bool::default(),
            prev_line_offset: u16::default(),
            debug: bool::default(),
        }
    }
}

impl Vm {
    /// Interns a string into the VM's dictionary's strings region.
    ///
    /// Strings stored in the string region are unique for now to avoid
    /// long-running executions from accumulating duplicate string entries.
    /// In the case that the given string is already present in the string
    /// region, the address of the already-existing string is used.
    ///
    /// TODO: Make this search not take O(n) time. Does Rust have ordered hash
    /// sets?
    fn intern_string(&mut self, s: String) -> (StrAddr, StrLen) {
        for (idx, existing) in self.dict.strings.iter().enumerate() {
            if s == *existing {
                return (idx, existing.len());
            }
        }

        let addr = self.dict.strings.len();
        let len = s.len();
        self.dict.strings.push(s);
        (addr, len)
    }

    fn handle_error(&mut self, err: VmError) -> VmErrorHandleResult {
        // TODO: Add logic for letting the user correct an error here.
        //
        // * The default behavior for an error should be Reset.
        // * Panic is reserved for undefined/unimplemented behavior or something
        // wacky like dictionary corruption or some shit.
        // * Continue indicates that the user (or some other system?) has
        // corrected the current error and the VM can assume it's safe to
        // resume execution at its error-site.
        VmErrorHandleResult::Panic
    }

    fn fetch_input(&mut self) {
        let line = self.rl_editor.readline("");
        match line {
            Ok(line) => {
                self.input = line;
                self.input_ptr = 0;
                self.prev_line_offset = 0;
            }
            Err(ReadlineError::Interrupted) => std::process::exit(0),
            Err(ReadlineError::Eof) => self.input = String::from(""),
            _ => panic!("readline error"),
        }
    }

    fn scan_word(&mut self) -> String {
        self.input
            .chars()
            .skip(self.input_ptr)
            .inspect(|_c| self.input_ptr += 1)
            .skip_while(|c| c.is_whitespace())
            .take_while(|c| !c.is_whitespace())
            .collect::<String>()
    }

    fn align_cursor_left(&mut self) {
        let (_, y) = self.stdout.0.cursor_pos().unwrap();
        write!(self.stdout.0, "{}", Goto(1, y)).unwrap();
        self.stdout.0.flush().unwrap();
    }

    /// Appends the prior terminal line with a status message right justified
    /// to TERM_WIDTH columns.
    fn attach_line_status(&mut self, status: String) {
        write!(self.stdout.0, "{Save}").unwrap();
        write!(self.stdout.0, "{}", Up(1)).unwrap();
        let (_, y) = self.stdout.0.cursor_pos().unwrap();
        write!(
            self.stdout.0,
            "{}",
            Goto(TERM_WIDTH - status.len() as u16 + 1, y)
        )
        .unwrap();
        write!(self.stdout.0, "{Italic}").unwrap();
        write!(self.stdout.0, "{status}").unwrap();
        write!(self.stdout.0, "{NoItalic}").unwrap();
        write!(self.stdout.0, "{Restore}").unwrap();
        self.stdout.0.flush().unwrap();
    }

    /// Appends the prior terminal line with the result of an evaluation.
    ///
    /// Unlike `attach_line_status` this method does not perform text
    /// justification.
    fn attach_line_result(&mut self, result: String) {
        write!(self.stdout.0, "{Save}").unwrap();
        write!(self.stdout.0, "{}", Up(1)).unwrap();
        write!(self.stdout.0, "{}", Right(1 + self.prev_line_offset)).unwrap();
        write!(self.stdout.0, "{Bold}").unwrap();
        write!(self.stdout.0, "{Invert}").unwrap();
        write!(self.stdout.0, "{result}").unwrap();
        write!(self.stdout.0, "{NoInvert}").unwrap();
        write!(self.stdout.0, "{NoBold}").unwrap();
        write!(self.stdout.0, "{Restore}").unwrap();
        self.prev_line_offset += 1 + result.len() as u16;
        self.stdout.0.flush().unwrap();
    }

    pub fn interpret(&mut self) {
        loop {
            let mut word: String;
            loop {
                word = self.scan_word();
                if !word.is_empty() {
                    break;
                }
                if self.show_status {
                    self.attach_line_status(match self.mode {
                        VmMode::Interpreting => String::from("ok"),
                        VmMode::Compiling => String::from("compiled"),
                    });
                } else {
                    self.show_status = true;
                }
                self.fetch_input();
            }
            if let Some(word_addr) = self.dict.lookup_word(&word) {
                let is_immediate = self.dict[word_addr].immediate;
                match (self.mode, is_immediate) {
                    (VmMode::Interpreting, _) | (_, true) => {
                        self.code_ptr = CodePtr {
                            index: word_addr,
                            ..Default::default()
                        };
                        self.execute();
                    }
                    (VmMode::Compiling, false) => {
                        let latest_idx = self.latest_word.unwrap();
                        let mut latest_xt = &mut self.dict[latest_idx].code;
                        (*latest_xt).push(Instruction::Forth(word_addr));
                    }
                }
            } else if let Ok(n) = word.parse::<VmWord>() {
                match self.mode {
                    VmMode::Interpreting => self.stack.push(n),
                    VmMode::Compiling => {
                        let latest_idx = self.latest_word.unwrap();
                        let mut latest_xt = &mut self.dict[latest_idx].code;
                        (*latest_xt).push(Instruction::Native(Op::Literal(n as VmWord)));
                    }
                }
            } else {
                self.attach_line_status(format!("Error: unrecognized word `{word}'"));
                self.show_status = false;
                continue;
            }
        }
    }

    /// Performs the instruction at the given code-pointer (an address in
    /// the dictionary's memory space).
    pub fn execute(&mut self) {
        use Op::*;

        macro_rules! try_handle_error {
            ($err:expr) => {
                match self.handle_error($err) {
                    VmErrorHandleResult::Panic => todo!(),
                    VmErrorHandleResult::Reset => todo!(),
                    VmErrorHandleResult::Continue => { /* continue */ }
                }
            };
        }

        macro_rules! assert_stack_size {
            ($n:literal) => {
                if self.stack.len() < $n {
                    try_handle_error!(VmError::StackUnderflow);
                }
            };
        }

        macro_rules! numeric_binop {
            ($op:tt) => {
                let rhs = self.stack.pop().unwrap();
                let lhs_mut = self.stack.top_mut().unwrap();
                *lhs_mut = *lhs_mut $op rhs;
            };
        }

        macro_rules! comparison_binop {
            ($op:tt) => {
                let rhs = self.stack.pop().unwrap();
                let lhs = *self.stack.top().unwrap();
                *self.stack.top_mut().unwrap() = if lhs $op rhs { TRUE } else { FALSE };
            };
        }

        /// Like numeric_binop but converts numbers to booleans according to
        /// Forth truthiness conventions.
        macro_rules! boolean_binop {
            ($op:tt) => {
                let rhs: bool = self.stack.pop().unwrap() != 0;
                let lhs: bool = *self.stack.top().unwrap() != 0;
                *self.stack.top_mut().unwrap() = if lhs $op rhs { TRUE } else { FALSE };
            };
        }

        loop {
            if self.debug {
                println!("\nCurrent op: {:?}", self.dict[self.code_ptr]);
            }
            let instr = self.dict[self.code_ptr];
            match instr {
                Instruction::Forth(next_addr) => {
                    self.return_stack.push(self.code_ptr);
                    self.code_ptr = CodePtr {
                        index: next_addr,
                        offset: 0,
                    };
                    continue;
                }
                Instruction::Native(op) => match op {
                    Shutdown => {
                        std::process::exit(0);
                    }
                    Exit => {
                        if self.return_stack.is_empty() {
                            return;
                        }
                        self.code_ptr = self.return_stack.pop().unwrap();
                    }
                    Dot => {
                        assert_stack_size!(1);
                        let result = format!("{}", self.stack.pop().unwrap());
                        self.attach_line_result(result);
                    }
                    Type => {
                        assert_stack_size!(2);
                        let len = self.stack.pop().unwrap();
                        let addr = self.stack.pop().unwrap() as StrAddr;
                        let string = &self.dict.strings[addr];
                        // TODO: Get rid of this clone.
                        self.attach_line_result(string.clone());
                    }
                    DotS => {
                        let result = format!("{}", self.stack);
                        self.attach_line_result(result);
                    }
                    Drop => {
                        assert_stack_size!(1);
                        self.stack.pop().unwrap();
                    }
                    Swap => {
                        assert_stack_size!(2);
                        let top_two = self.stack.top_n_mut(2).unwrap();
                        top_two.swap(0, 1);
                    }
                    Dup => {
                        assert_stack_size!(1);
                        let top = self.stack.top().unwrap();
                        self.stack.push(*top);
                    }
                    Rot => {
                        assert_stack_size!(3);
                        let top_three = self.stack.top_n_mut(3).unwrap();
                        top_three.swap(0, 1);
                        top_three.swap(1, 2);
                    }
                    Over => {
                        assert_stack_size!(2);
                        let elem = self.stack.top_n(2).unwrap()[0];
                        self.stack.push(elem);
                    }
                    Add => {
                        assert_stack_size!(2);
                        numeric_binop!(+);
                    }
                    Sub => {
                        assert_stack_size!(2);
                        numeric_binop!(-);
                    }
                    Mul => {
                        assert_stack_size!(2);
                        numeric_binop!(*);
                    }
                    Div => {
                        assert_stack_size!(2);
                        numeric_binop!(/);
                    }
                    Mod => {
                        assert_stack_size!(2);
                        numeric_binop!(%);
                    }
                    Pow => {
                        assert_stack_size!(2);
                        let exp = self.stack.pop().unwrap();
                        let tos = self.stack.top_mut().unwrap();
                        // TODO: Verify that downcasting from isize to u32 is safe.
                        // Should be?
                        *tos = tos.pow(exp as u32);
                    }
                    Eq => {
                        assert_stack_size!(2);
                        comparison_binop!(==);
                    }
                    EqZero => {
                        assert_stack_size!(1);
                        let tos = self.stack.top_mut().unwrap();
                        *tos = if *tos == 0 { TRUE } else { FALSE };
                    }
                    LessThan => {
                        assert_stack_size!(2);
                        comparison_binop!(<);
                    }
                    LessThanEq => {
                        assert_stack_size!(2);
                        comparison_binop!(<=);
                    }
                    GreaterThan => {
                        assert_stack_size!(2);
                        comparison_binop!(>);
                    }
                    GreaterThanEq => {
                        assert_stack_size!(2);
                        comparison_binop!(>=);
                    }
                    Not => {
                        assert_stack_size!(1);
                        let tos = self.stack.top_mut().unwrap();
                        let tos_bool = *tos != 0;
                        *tos = if !tos_bool { TRUE } else { FALSE };
                    }
                    And => {
                        assert_stack_size!(2);
                        boolean_binop!(&&);
                    }
                    Or => {
                        assert_stack_size!(2);
                        boolean_binop!(||);
                    }
                    Shl => {
                        assert_stack_size!(2);
                        numeric_binop!(<<);
                    }
                    Shr => {
                        assert_stack_size!(2);
                        numeric_binop!(>>);
                    }
                    BinNot => {
                        assert_stack_size!(1);
                        let tos = self.stack.top_mut().unwrap();
                        *tos = !*tos;
                    }
                    BinAnd => {
                        assert_stack_size!(2);
                        numeric_binop!(&);
                    }
                    BinOr => {
                        assert_stack_size!(2);
                        numeric_binop!(|);
                    }
                    BinXor => {
                        assert_stack_size!(2);
                        numeric_binop!(^);
                    }
                    PlaceJumpMarker => {
                        if self.mode != VmMode::Compiling {
                            try_handle_error!(VmError::CompileOnlyWord);
                        }
                        let latest_ptr = self.latest_word.unwrap();
                        let mut latest_xt = &mut self.dict[latest_ptr].code;
                        (*latest_xt).push(Instruction::Native(Op::JumpInstr(
                            JumpInstruction::AwaitingOffset,
                        )));
                    }
                    FillJumpMarker => {
                        if self.mode != VmMode::Compiling {
                            try_handle_error!(VmError::CompileOnlyWord);
                        }
                        let latest_ptr = self.latest_word.unwrap();
                        let mut i = 0;
                        let current_position = self.dict[latest_ptr].code.len() - 1;
                        loop {
                            let instr = &mut self.dict[latest_ptr].code[current_position - i];
                            if *instr
                                == Instruction::Native(JumpInstr(JumpInstruction::AwaitingOffset))
                            {
                                *instr =
                                    Instruction::Native(JumpInstr(JumpInstruction::JumpIfZero(i)));
                                break;
                            }

                            i += 1;
                        }
                    }
                    JumpInstr(JumpInstruction::AwaitingOffset) => {
                        try_handle_error!(VmError::UnreachableWord);
                    }
                    JumpInstr(JumpInstruction::JumpIfZero(offset)) => {
                        assert_stack_size!(1);
                        if self.stack.pop().unwrap() == 0 {
                            self.code_ptr.offset += offset;
                        }
                    }
                    Here => {
                        // No-op word. This word exists to give greater clarity
                        // to code when debugging dictionaries.
                    }
                    Literal(n) => self.stack.push(n as VmWord),
                    LiteralFromStack => {
                        if self.mode != VmMode::Compiling {
                            try_handle_error!(VmError::CompileOnlyWord);
                        }
                        assert_stack_size!(1);
                        let latest_ptr = self.latest_word.unwrap();
                        let mut latest_xt = &mut self.dict[latest_ptr].code;
                        (*latest_xt)
                            .push(Instruction::Native(Op::Literal(self.stack.pop().unwrap())));
                    }
                    Define => {
                        let word = self.scan_word();
                        if word.is_empty() {
                            try_handle_error!(VmError::ZeroLengthWord);
                        }

                        self.mode = VmMode::Compiling;
                        self.latest_word = Some(self.dict.words.len());
                        self.dict.words.push(Word {
                            name: word,
                            code: vec![],
                            data: vec![],
                            immediate: false,
                            hidden: false,
                        });
                    }
                    EndDefine => {
                        let latest_ptr = self.latest_word.unwrap();
                        let mut latest_xt = &mut self.dict[latest_ptr].code;
                        (*latest_xt).push(Instruction::Native(Op::Exit));
                        let word_name = &self.dict[latest_ptr].name;
                        self.attach_line_status(format!("Defined word `{word_name}'"));
                        self.show_status = false;
                        self.latest_word = None;
                        self.mode = VmMode::Interpreting;
                    }
                    InterpretMode => {
                        // TODO: Make this more robust. Eg, this should fail but doesnt:
                        // : foo [ ;
                        // Should probably make this a parsing word that seeks until `]`
                        // and does the interpretation inline.
                        if self.mode != VmMode::Compiling {
                            try_handle_error!(VmError::CompileOnlyWord);
                        }
                        self.mode = VmMode::Interpreting;
                    }
                    EndInterpretMode => {
                        if self.mode != VmMode::Interpreting || self.latest_word.is_none() {
                            try_handle_error!(VmError::CompileOnlyWord);
                        }
                        self.mode = VmMode::Compiling;
                    }
                    ParseWord => {
                        // TODO: Should we just convert the input buffer to a
                        // Vec<char> instead of a String to make processing easier?
                        let mut word: String;
                        loop {
                            word = self.scan_word();
                            if !word.is_empty() {
                                break;
                            }
                            self.fetch_input();
                        }

                        let (addr, len) = self.intern_string(word);
                        self.stack.push(addr as VmWord);
                        self.stack.push(len as VmWord);
                    }
                    Find => {
                        // Expect ( addr len ) on stack
                        assert_stack_size!(2);
                        self.stack.pop(); // Don't need the length
                        let addr = self.stack.pop().unwrap() as StrAddr;

                        if self.dict.strings.len() <= addr {
                            try_handle_error!(VmError::InvalidStringIndex);
                        }

                        let word_name = &self.dict.strings[addr];
                        match self.dict.lookup_word(word_name) {
                            None => {
                                try_handle_error!(VmError::UnknownWord);
                            }
                            Some(word_addr) => self.stack.push(word_addr as VmWord),
                        }
                    }
                },
            }
            self.code_ptr.offset += 1;
        }
    }
}

fn main() {
    macro_rules! op_word {
        ($name:literal, $op:expr) => {
            Word {
                name: String::from($name),
                code: vec![Instruction::Native($op), Instruction::Native(Op::Exit)],
                data: vec![],
                immediate: false,
                hidden: false,
            }
        };
    }

    macro_rules! op_word_immediate {
        ($name:literal, $op:expr) => {
            Word {
                name: String::from($name),
                code: vec![Instruction::Native($op), Instruction::Native(Op::Exit)],
                data: vec![],
                immediate: true,
                hidden: false,
            }
        };
    }

    let dict = Dictionary {
        words: vec![
            op_word!("bye", Op::Shutdown),
            op_word!("exit", Op::Exit),
            op_word!(".", Op::Dot),
            op_word!("type", Op::Type),
            op_word!(".s", Op::DotS),
            op_word!("drop", Op::Drop),
            op_word!("swap", Op::Swap),
            op_word!("dup", Op::Dup),
            op_word!("rot", Op::Rot),
            op_word!("over", Op::Over),
            op_word!("+", Op::Add),
            op_word!("-", Op::Sub),
            op_word!("*", Op::Mul),
            op_word!("/", Op::Div),
            op_word!("mod", Op::Mod),
            op_word!("**", Op::Pow),
            op_word!("=", Op::Eq),
            op_word!("0=", Op::EqZero),
            op_word!("<", Op::LessThan),
            op_word!("<=", Op::LessThanEq),
            op_word!(">", Op::GreaterThan),
            op_word!(">=", Op::GreaterThanEq),
            op_word!("not", Op::Not),
            op_word!("and", Op::And),
            op_word!("or", Op::Or),
            op_word!("<<", Op::Shl),
            op_word!(">>", Op::Shr),
            op_word!("~", Op::BinNot),
            op_word!("&", Op::BinAnd),
            op_word!("|", Op::BinOr),
            op_word!("^", Op::BinXor),
            op_word!(":", Op::Define),
            op_word_immediate!(";", Op::EndDefine),
            op_word_immediate!("[", Op::InterpretMode),
            op_word_immediate!("]", Op::EndInterpretMode),
            op_word_immediate!("literal", Op::LiteralFromStack),
            Word {
                name: String::from("]l"),
                code: vec![
                    Instruction::Native(Op::EndInterpretMode),
                    Instruction::Native(Op::LiteralFromStack),
                    Instruction::Native(Op::Exit),
                ],
                data: vec![],
                immediate: true,
                hidden: false,
            },
            op_word!("word", Op::ParseWord),
            op_word!("find", Op::Find),
            op_word_immediate!("if", Op::PlaceJumpMarker),
            op_word_immediate!("then", Op::FillJumpMarker),
        ],
        ..Dictionary::default()
    };

    let mut vm = Vm {
        dict,
        input: String::from(""),
        debug: false,
        ..Vm::default()
    };
    println!("{:=^80}", "PL/6 Interactive Environment");
    vm.interpret();
}
