use crate::CodePtr;
use crate::VmWord;

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum JumpInstruction {
    AwaitingOffset,
    JumpIfZero(usize),
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum Op {
    Shutdown,
    Exit,

    Dot,
    Type,
    DotS,

    Drop,
    Swap,
    Dup,
    Rot,
    Over,

    Add,
    Sub,
    Mul,
    Div,
    Mod,
    Pow,

    Eq,
    EqZero,
    LessThan,
    LessThanEq,
    GreaterThan,
    GreaterThanEq,

    Not,
    And,
    Or,

    Shl,
    Shr,
    BinNot,
    BinAnd,
    BinOr,
    BinXor,

    PlaceJumpMarker,
    FillJumpMarker,
    JumpInstr(JumpInstruction),

    /// No-op word. This word exists to give greater clarity to code when
    /// debugging dictionaries.
    Here,

    Literal(VmWord),

    /// Consumes a literal from the stack and compiles it into the current word
    /// definition. (Compile-time only operation).
    LiteralFromStack,

    Define,
    EndDefine,

    /// Toggles the VM back to interpret mode (Compile-time only operation).
    InterpretMode,
    /// Toggles the VM into compile mode from interpret mode (Compile-time only operation).
    EndInterpretMode,

    /// Parses a word (a chunk of text delimited by whitespace) starting at the
    /// current point in the input buffer.
    ParseWord,

    /// Given a string (addr len) on the stack, attempts to look up that string
    /// as a word in the dictionary. If the word is found its code pointer will
    /// be pushed onto the stack.
    Find,
}
