use std::fmt;
use std::fmt::Display;
use std::ops::{Index, IndexMut};

/// The stack which holds the working memory of the Forth VM.
#[derive(Clone, Debug)]
pub struct Stack<T>(Vec<T>);

// NOTE: This trait is implemented to support potential low-level operations
// but in general it is best to avoid using it when one of the types functions
// can achieve the same thing.
impl<T, S> Index<T> for Stack<S>
where
    T: Into<usize>,
{
    type Output = S;
    fn index(&self, idx: T) -> &Self::Output {
        &self.0[idx.into()]
    }
}

// NOTE: This trait is implemented to support potential low-level operations
// but in general it is best to avoid using it when one of the types functions
// can achieve the same thing.
impl<T, S> IndexMut<T> for Stack<S>
where
    T: Into<usize>,
{
    fn index_mut(&mut self, idx: T) -> &mut Self::Output {
        &mut self.0[idx.into()]
    }
}

impl<T: std::fmt::Display> Display for Stack<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "<{}>", self.0.len())?;
        for elem in self.0.iter() {
            write!(f, " {elem}")?;
        }
        Ok(())
    }
}

impl<T> Default for Stack<T> {
    fn default() -> Stack<T> {
        Stack(vec![])
    }
}

impl<T> Stack<T> {
    /// Creates a new instance of a stack.
    pub fn new() -> Stack<T> {
        Stack::default()
    }

    /// Returns the length of the given stack.
    pub fn len(&self) -> usize {
        self.0.len()
    }

    /// Returns whether the given stack is empty.
    pub fn is_empty(&self) -> bool {
        self.0.is_empty()
    }

    /// Clears the given stack of all its elements.
    ///
    /// Does not affect the total capacity of the stack.
    pub fn clear(&mut self) {
        self.0.clear();
    }

    /// Pushes a single value onto the top of the given stack.
    pub fn push(&mut self, word: T) {
        self.0.push(word);
    }

    /// Pops a single value off the top of the stack and returns it, if the
    /// stack is non-empty.
    pub fn pop(&mut self) -> Option<T> {
        self.0.pop()
    }

    /// Returns the value of the top of the stack. Equivalent to a "peek" but
    /// named top since peeks conventionally do not support mutations like
    /// `top_mut` below.
    pub fn top(&self) -> Option<&T> {
        self.0.last()
    }

    /// Returns a mutable reference to the top of the given stack.
    ///
    /// Intended for use as an optimization over popping a value from the stack
    /// and immediately pushing another.
    pub fn top_mut(&mut self) -> Option<&mut T> {
        self.0.last_mut()
    }

    /// Returns a slice of the top N elements of the given stack.
    ///
    /// Elements are ordered in the same way the stack is are represented when
    /// printed, ie the leftmost (0th) element is the deepest stack element and
    /// the rightmost (Nth) element is the top of the stack.
    pub fn top_n(&self, n: usize) -> Option<&[T]> {
        let len = self.len();
        if len < n {
            None
        } else {
            let hi = len - 1;
            let lo = hi - (n - 1);
            Some(&self.0[lo..hi])
        }
    }

    /// Returns a mutable slice of the top N elements of the given stack.
    ///
    /// Elements are ordered in the same way the stack is are represented when
    /// printed, ie the leftmost (0th) element is the deepest stack element and
    /// the rightmost (Nth) element is the top of the stack.
    ///
    /// Intended for use as an optimization over popping multiple values from
    /// and to the stack for operations that do not affect the total number
    /// of stack elements.
    pub fn top_n_mut(&mut self, n: usize) -> Option<&mut [T]> {
        let len = self.len();
        if len < n {
            None
        } else {
            let hi = len;
            let lo = hi - n;
            Some(&mut self.0[lo..hi])
        }
    }
}
