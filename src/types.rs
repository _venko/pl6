use std::fmt;
use std::fmt::Display;

pub type VmWord = isize;

pub type WordAddr = usize;
pub type StrAddr = usize;
pub type StrLen = usize;

/// A pointer into the code segments of a dictionary's address space.
///
/// Since code instructions are stored by words, a code pointer uses
/// an index, which maps the dictionary's word map to a particular word, and
/// an offset, which is an index into that word's code segment.
#[derive(Debug, Default, Copy, Clone, PartialEq, Eq)]
pub struct CodePtr {
    pub index: usize,
    pub offset: usize,
}

impl Display for CodePtr {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "c[{},{}]", self.index, self.offset)
    }
}
