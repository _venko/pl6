: TO-STRING ( n -- addr len )             \ Converts a number to a string.
    s>d <# #s #> ;

: TYPE-NUM  ( n -- )                      \ Prints a number after converting it to a string.
    TO-STRING TYPE ;                      \ This is similar to the . word but prevents a trailing
                                          \ space from being printed after the number itself.

: POINT2  ( x y -- Point(x,y) )           \ Allocates memory for a 2D point and assigns as its x
    SWAP CREATE , ,                       \ and y values the two numbers present on the stack.
    DOES> DUP @ SWAP 1 CELLS + @ ;

: AS                                      \ Alias for POINT2 so you can bind the results of point
    POINT2 ;                              \ arithmetic operations to names in a clean way

: POINT2+ ( ax ay bx by -- ax+bx ay+by )  \ Adds two points together and places the resulting pair
    ROT 2SWAP + -ROT + ;                  \ on the stack.

: .POINT2  ( x y -- )                     \ Pretty-prints a point
    SWAP
    ." POINT2(" TYPE-NUM ." , " TYPE-NUM ." )" ;


3 4 POINT2 u   \ u = Point2(3, 4)
5 6 POINT2 v   \ v = Point2(5, 6)
u v POINT2+
  AS result    \ result = u + v

result .POINT BYE
